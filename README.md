# ADPluginCalib conda recipe

Home: "https://github.com/epics-modules/ADPluginCalib"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ADPluginCalib module
